#!/bin/bash

set_password() {
  local user=$1
  local user_password=$2
  local ca_crt=$3
  local endpoint=$4
  local elastic_password=$5
  echo "Setting $user password";
  until curl -s -X POST --cacert $ca_crt -u elastic:$elastic_password -H "Content-Type: application/json" $endpoint/_security/user/$user/_password \
  -d "{\"password\":\"${user_password}\"}" \
  | grep -q "^{}"; do sleep 10; done;

}

create_role() {
  local role=$1
  local role_indices=$2
  local ca_crt=$3
  local endpoint=$4
  local elastic_password=$5
  echo "Creating $role role";
  until curl -s -X POST --cacert $ca_crt -u elastic:$elastic_password -H "Content-Type: application/json" $endpoint/_security/role/$role \
  -d '{"cluster":["manage_index_templates","monitor","manage_ilm"],"indices":[{"names":["'$role_indices'"],"privileges":["write","create","create_index","manage","manage_ilm"]}]}' \
  | grep "role"; do sleep 10; done;

}

create_user() {
  local user=$1
  local user_password=$2
  local role=$3
  local ca_crt=$4
  local endpoint=$5
  local elastic_password=$6
  # Create logstash_internal user
  echo "Creating logstash_writer user";
  until curl -s -X POST --cacert $ca_crt -u elastic:$elastic_password -H "Content-Type: application/json" $endpoint/_security/user/$user \
  --data @<(cat <<EOF 
{
  "password" : "$user_password",
  "roles" : ["$role"],
  "full_name" : "$user"
}
EOF
) \
  | grep "created"; do sleep 10; done;

}

# Check .env variables exist
for variable in ELASTIC_PASSWORD KIBANA_SYSTEM_PASSWORD LOGSTASH_SYSTEM_PASSWORD BEATS_SYSTEM_PASSWORD LOGSTASH_WRITER_PASSWORD FILEBEAT_WRITER_PASSWORD
do
  if [[ -z "${!variable}" ]]; then
    echo "Set the $variable environment variable in the project .env file";
    exit 1
  fi
done

# Setup certs
if ! /setup/certs/certs.sh; then
    exit 1
fi

# Set Variables
CA_CRT=/certs/https-ca/ca/ca.crt
ENDPOINT=https://es01:9200

# Wait for elasticsearch availability
echo "Waiting for Elasticsearch availability";
until curl -s --cacert ${CA_CRT} ${ENDPOINT} | grep -q "missing authentication credentials"; do sleep 30; done;

# Set system passwords
set_password kibana_system ${KIBANA_SYSTEM_PASSWORD} ${CA_CRT} ${ENDPOINT} ${ELASTIC_PASSWORD}
set_password logstash_system ${LOGSTASH_SYSTEM_PASSWORD} ${CA_CRT} ${ENDPOINT} ${ELASTIC_PASSWORD}
set_password beats_system ${BEATS_SYSTEM_PASSWORD} ${CA_CRT} ${ENDPOINT} ${ELASTIC_PASSWORD}

# Create logstash_writer roler and user
create_role logstash_writer 'logstash-*' ${CA_CRT} ${ENDPOINT} ${ELASTIC_PASSWORD}
create_user logstash_writer ${LOGSTASH_WRITER_PASSWORD} logstash_writer ${CA_CRT} ${ENDPOINT} ${ELASTIC_PASSWORD}

# Create filebeat_writer roler and user
create_role filebeat_writer 'filebeat-*' ${CA_CRT} ${ENDPOINT} ${ELASTIC_PASSWORD}
create_user filebeat_writer ${FILEBEAT_WRITER_PASSWORD} filebeat_writer ${CA_CRT} ${ENDPOINT} ${ELASTIC_PASSWORD}

# Finished
echo "All done!";
