#!/bin/bash

function create_ca() {
  CA_NAME=$1-ca
  CA_ZIP=$CA_NAME.zip
  # If key doesn't exist
  if [ ! -f "/certutil/$CA_NAME/ca/ca.key" ]; then
    # If zip doesn't exist
    if [ ! -f "/certutil/$CA_ZIP" ]; then
      echo "Creating $CA_NAME";
      bin/elasticsearch-certutil ca --silent --pem -out /certutil/$CA_ZIP;
    fi
    mkdir -p /certs/$CA_NAME/ca/;
    unzip /certutil/$CA_ZIP -d /certutil/$CA_NAME;
    cp /certutil/$CA_NAME/ca/ca.crt /certs/$CA_NAME/ca/ca.crt;
  else
    echo "Setup has already been run on $CA_NAME";
  fi;

}

function create_certificates() {
  CONFIG_FILE=$1.yml
  CA_NAME=$1-ca
  CERTIFICATES_NAME=$1-certs
  CERTIFICATES_ZIP=$CERTIFICATES_NAME.zip
  # Remove existing certificates
  echo "Removing  $CERTIFICATES_NAME";
  rm -f /certutil/$CERTIFICATES_ZIP
  find /certs/$CERTIFICATES_NAME/ -type f -exec rm -r {} \;
  # Create certificates from CA
  echo "Creating $CERTIFICATES_NAME";
  bin/elasticsearch-certutil cert --silent --pem --ca-cert /certutil/$CA_NAME/ca/ca.crt --ca-key /certutil/$CA_NAME/ca/ca.key --in /setup/certs/instances/$CONFIG_FILE -out /certutil/$CERTIFICATES_ZIP;
  # Extract to certs folder
  unzip /certutil/$CERTIFICATES_ZIP -d /certs/$CERTIFICATES_NAME;

}

function set_permissions() {
  for folder in /certs/$1-ca /certs/$1-certs
  do 
    echo "Setting permissions on $folder";
    chown -R 1000:0 $folder;
    find $folder -type d -exec chmod 750 \{\} \;;
    find $folder -type f -exec chmod 640 \{\} \;;
  done
}

# Loop on config files
for CONFIG_FILE in /setup/certs/instances/*.yml; do
  [ -e "$CONFIG_FILE" ] || continue
  CONFIG_NAME=$(basename "${CONFIG_FILE%.*}")
  # Create CA
  if ! create_ca $CONFIG_NAME; then
    exit 1
  fi
  # Create Certificates from CA
  if ! create_certificates $CONFIG_NAME $; then
    exit 1
  fi
  # Set permissions on CA and Certificates
  if ! set_permissions $CONFIG_NAME; then
    exit 1
  fi
done
